<?php

namespace App\Command;

use App\Entity\Film;
use App\Repository\FilmRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManager;

class AddFilmCommand extends Command
{
    protected static $defaultName = 'app:add-film';
    /**
     * @var FilmRepository
     */
    private $filmRepository;
    /**
     * @var EntityManager
     */
    private $em;
    
    public function __construct(ContainerInterface $container)
    {
        parent::__construct();
        $this->em = $container->get('doctrine')->getManager();
        $this->filmRepository = $this->em->getRepository(Film::class);
    }
    
    protected function configure()
    {
        $this
            ->setDescription('Add a film to the database')
            ->addArgument('title', InputArgument::REQUIRED, 'title of the film')
            ->addArgument('year', InputArgument::REQUIRED, 'year of release')
//            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
//         $io = new SymfonyStyle($input, $output);
//         $arg1 = $input->getArgument('arg1');

//         if ($arg1) {
//             $io->note(sprintf('You passed an argument: %s', $arg1));
//         }

//         if ($input->getOption('option1')) {
//             // ...
//         }

//         $io->success('You have a new command! Now make it your own! Pass --help to see your options.');
        $title = $input->getArgument('title');
        $year = $input->getArgument('year');
        
        if($year && ! preg_match( '/^\d+$/', $year ) ) {
            $output->writeln('<error>second argument must be integer</error>');
            exit(2);
        }
        
        $film = $this->filmRepository->findOneBy(
            ['year' => $year,
                'title' => $title]);
            if($film) {
                $output->writeln('film already present: ' . $title . ' (' . $year .')');
                exit(1);
            }
        
         $film = new Film();
         $film->setTitle($title);
         $film->setYear($year);
         
         $this->em->persist($film);
         $this->em->flush();
    }
}
