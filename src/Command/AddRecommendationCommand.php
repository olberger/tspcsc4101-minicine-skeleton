<?php

namespace App\Command;

use App\Entity\Film;
use App\Repository\FilmRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Console\Question\Question;
use App\Entity\Recommendation;

class AddRecommendationCommand extends Command
{
    protected static $defaultName = 'app:add-recommendation';
    /**
     * @var FilmRepository
     */
    private $filmRepository;
    /**
     * @var EntityManager
     */
    private $em;
    
    public function __construct(ContainerInterface $container)
    {
        parent::__construct();
        $this->em = $container->get('doctrine')->getManager();
        $this->filmRepository = $this->em->getRepository(Film::class);
    }
    protected function configure()
    {
        $this
            ->setDescription('Interactive dialog to add a recommendation to a film')
        //             ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
//             ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $helper = $this->getHelper('question');
        $films = $this->filmRepository->findAll();
        $films = array_map('strval', $films);

        $film = null;
        do {
        
            $output->writeln('Please enter the name (and date) of a film');
            $question = new Question("('?' for a list - autocompletion active - RETURN to abort): ", '');
            $question->setAutocompleterValues($films);
            
            $filmName = $helper->ask($input, $output, $question);
            
            if($filmName == "?") {
                $output->writeln($films);
            }
            else {
                if(in_array($filmName, $films)) {
                    
                    preg_match('/(.*) \((\d+)\)/', $filmName, $m);
                    $title = $m[1];
                    $year = $m[2];
                   
                    $film = $this->filmRepository->findOneBy(
                        ['year' => $year,
                         'title' => $title]);
                }
                else {
                    if($filmName != "") {
                        $output->writeln('Not found: '. $filmName);
                    }
                }
            }
        } while (($film == null) && ($filmName != ""));
        
        if($film) {
            $question = new Question("Enter your recommendation: ", '');
            
            $recotext = $helper->ask($input, $output, $question);
            $output->writeln($recotext);
            
            $recommendation = new Recommendation();
            $recommendation->setRecommendation($recotext);
            $film->addRecommendation($recommendation);
            
            $this->em->persist($film);
            $this->em->flush();
        }
    }
}
