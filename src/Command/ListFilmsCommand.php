<?php

namespace App\Command;

use App\Entity\Film;
use App\Repository\FilmRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
//use Symfony\Component\Console\Style\SymfonyStyle;

class ListFilmsCommand extends Command
{
    protected static $defaultName = 'app:list-films';
    
    /**
     * @var FilmRepository
     */
    private $filmRepository;
    
    public function __construct(ContainerInterface $container)
    {
        parent::__construct();
        $this->filmRepository = $container->get('doctrine')->getManager()->getRepository(Film::class);
    }
    
    protected function configure()
    {
        $this
            ->setDescription('List films')
            ->addArgument('year', InputArgument::OPTIONAL, 'Filter films of a single year')
            ->addOption('unique', null, InputOption::VALUE_NONE, 'Avoid listing remakes')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        //$io = new SymfonyStyle($input, $output);
        $year = $input->getArgument('year');
        
        if ($year) {
            if(! preg_match( '/^\d+$/', $year ) ) {
                $output->writeln('<error>argument must be integer</error>');
                exit(2);
            }
            $films = $this->filmRepository->findBy(
                ['year' => $year],
                ['title' => 'ASC']);
            if(!$films) {
                $output->writeln('<comment>no films found in ' . $year .'<comment>');
                exit(1);
            }
        }
        else {
            $films = $this->filmRepository->findAll();
            if(!$films) {
                $output->writeln('<comment>no films found<comment>');
                exit(1);
            }
        }
        
        if ($input->getOption('unique')) {
            $titles = array();
            foreach($films as $film) {
                $titles[] = $film->getTitle();
            }
            $output->writeln(array_unique($titles));
        }
        else {
            foreach($films as $film)
            {
                $output->writeln($film);
            }
        }
    }
}
