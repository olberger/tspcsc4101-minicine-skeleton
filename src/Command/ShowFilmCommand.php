<?php

namespace App\Command;

use App\Entity\Film;
use App\Repository\FilmRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
//use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
//use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ShowFilmCommand extends Command
{
    protected static $defaultName = 'app:show-film';
    
    /**
     * @var FilmRepository
     */
    private $filmRepository;
    
    public function __construct(ContainerInterface $container)
    {
        parent::__construct();
        $this->filmRepository = $container->get('doctrine')->getManager()->getRepository(Film::class);
    }
    
    protected function configure()
    {
        $this
            ->setDescription('Show recommendations for a film')            
            ->addArgument('title', InputArgument::REQUIRED, 'Title of the film (spaces must be quoted)')
            ->addArgument('year', InputArgument::OPTIONAL, 'Year of the film')
        ;
    }

    protected static function outputFilmRecommendations(OutputInterface $output, Film $film, $prefix = '') 
    {
        foreach($film->getRecommendations() as $recommendation) {
            $output->writeln($prefix . "\t" . $recommendation);
        }
    }
    
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $title = $input->getArgument('title');
        $year = $input->getArgument('year');
        
        if($year && ! preg_match( '/^\d+$/', $year ) ) {
            $output->writeln('<error>second argument must be integer</error>');
            exit(2);
        }
                
        if($year)
        {
            $film = $this->filmRepository->findOneBy(
                ['year' => $year,
                 'title' => $title]);
            if(!$film) {
                $output->writeln('unknown film: ' . $title . ' (' . $year .')');
                exit(1);
            }
            $output->writeln($film . ':');
            self::outputFilmRecommendations($output, $film);
        }
        else {
            $films = $this->filmRepository->findBy(
                ['title' => $title],
                ['year' => 'ASC']);
            if(empty($films)) {
                $output->writeln('unknown film: ' . $title);
                exit(1);
            }
            if(count($films) > 1) {
                $output->writeln($title . ':');
                
                foreach($films as $film) {
                    $output->writeln("\t" . $film->getYear() . ':');
                    self::outputFilmRecommendations($output, $film, "\t");
                }
            }
            else {
                $film = $films[0];
                $output->writeln($film . ':');
                self::outputFilmRecommendations($output, $film);
            }
        }
    }
}
