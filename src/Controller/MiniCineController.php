<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class MiniCineController extends AbstractController
{
    /**
     * List all films in the DB (may limit to a year)
     * 
     * @Route("/{year}", name="films_index", requirements={"year"="\d+"})
     */
    public function index($year = null)
    {
        $res = "Liste des films ";
        if($year) {
            $res .= "de " . $year . " :";
            $films = $this->getDoctrine()->getRepository('App:Film')->findBy(
                ['year' => $year],
                ['title' => 'ASC']);
        }
        else {
            $res .= "complète :";
            $films = $this->getDoctrine()->getRepository('App:Film')->findAll();
        }
        ":<ul>";
        
        foreach ($films as $film)
        {
            $res = $res . "<li>"; 
                
            $url = $this->generateUrl(
                'film_show', 
                ['title' => $film->getTitle(),
                 'year' => $film->getYear()]);
            
            $res .= '<a href="' . $url .  '">' . $film . "</a>";
            
            $res .= "</li>";
        }
        $res .= '</ul>';
        if($year) {
            $res .= '<p/><a href="' . $this->generateUrl('films_index') . '">Back</a>';
        }
        return new Response('<html><body>'. $res . '</body></html>');
    }

    /**
     * List all remakes of the same film
     *
     * @Route("/{title}", name="remakes")
     */
    public function remakes($title)
    {
        $films = $this->getDoctrine()->getRepository('App:Film')->findBy(
            ['title' => $title],
            ['year' => 'ASC']
         );
        
        if (count($films) == 0) {
            throw $this->createNotFoundException('The film does not exist');
        }
        
        $res = 'Liste des versions du film "' . $title . '" :<ul>';
        
        foreach ($films as $film)
        {
            $res = $res . "<li>";
            
            $url = $this->generateUrl(
                'film_show',
                ['title' => $film->getTitle(),'year' => $film->getYear()]);
            
            $res .= '<a href="' . $url .  '">' . $film . "</a>";
            
            $res .= "</li>";
        }
        $res .= '</ul>';
        
        $res .= '<p/><a href="' . $this->generateUrl('films_index') . '">Back</a>';
        
        return new Response('<html><body>'. $res . '</body></html>');
    }
    
    /**
     * Show a film and its recommendations
     * 
     * @Route("/{title}/{year}", name="film_show", requirements={"year"="\d+"})
     *    note that the year must be an integer, above
     *    
     * @param String $title
     * @param Integer $year
     */
    public function show($title, $year)
    {
        $filmRepo = $this->getDoctrine()->getRepository('App:Film');
        $film = $filmRepo->findOneBy(['title' => $title, 'year' => $year]);
        
        if (!$film) {
            throw $this->createNotFoundException('The film does not exist');
        }
        
        $urlremakes = $this->generateUrl('remakes', ['title' => $film->getTitle()]);
        $urlyear = $this->generateUrl('films_index', ['year' => $film->getYear()]);
        
        $recommendations = $film->getRecommendations();
        
        if (count($recommendations))
        {
            $res = "Recommendations pour le film " . 
                '<a href="' . $urlremakes .'">'. $film->getTitle() . "</a> de ". 
                '<a href="' . $urlyear . '">' . $film->getYear() . "</a> :";
            
            $res = $res . "<ul>";
            foreach($recommendations as $recommendation)
            {
                $res .= "<li>" . $recommendation . "</li>";
            }
            $res .= "</ul>";
        }
        else 
       {
           $res = "Désolé, aucune recommendation pour le film " .
               '<a href="' . $urlremakes .'">'. $film->getTitle() . "</a> de ".
               '<a href="' . $urlyear . '">' . $film->getYear() . "</a>.";
        }
        
        $res .= '<p/><a href="' . $this->generateUrl('films_index') . '">Back</a>';
        
        return new Response('<html><body>'. $res . '</body></html>');
    }
}
